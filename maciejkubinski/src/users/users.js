const users = angular.module("users", ["users.service"]);

users.component("usersPage", {
  bindings: {},
  template: /* html */ `
  <div class="container">
    <div class="row">
      <div class="col-5">
      <users-list users="$ctrl.users"></users-list>
      </div>
    </div>
  </div>
  `,
  controller(UsersService) {
    this.draft = {};
    this.isEditingUser = false;
    this.users = [];
    this.selected = null;

    this.refresh = () => {
      UsersService.fetchUsers().then((data) => {
        this.users = data;
      });
    };
    this.refresh()

    this.select = (id) => {
      if (this.selected && this.selected.id === id) {
        this.selected = null;
      } else {
        UsersService.fetchUserById(id).then((data) => {
          this.selected = data;
          this.draft = { ...this.selected };
        });
      }
      this.cancelEditUser();
    };

    this.save = (draft) => {
      UsersService.updateUser(draft);
      this.cancelEditUser();
      this.selected = null;
      this.refresh();
    };

    this.editUser = () => {
      this.isEditingUser = true;
    };

    this.cancelEditUser = () => {
      this.isEditingUser = false;
    };

    this.deleteUser = (id) => {
      UsersService.deleteUser(id);
      this.refresh();
      this.selected = null;
      this.cancelEditUser();
    };
  },
});


// ng-click="UserCtrl.select(user.id)" ng-class="{'active': user.id === UserCtrl.selected.id}"
users.component("usersList", {
  bindings: {users: "="},
  template: /* html */ `
  <h4>Users List</h4>
  <div class="list-group">
    <div class="list-group-item" ng-repeat="user in $ctrl.users" >{{user.username}}</div>
  </div>
  `,
  controller() {},
});

users.component("usersDetails", {
  bindings: {},
  template: /* html */ `
  <h1>Users</h1>
  `,
  controller() {},
});

users.component("usersEditform", {
  bindings: {},
  template: /* html */ `
  <h1>Users</h1>
  `,
  controller() {},
});

// users.controller('UserCtrl', ($scope, UsersService) => {
//   const vm = $scope.UserCtrl ={}
//   vm.selected = null
//   vm.draft = {}
//   vm.isEditingUser = false
//   vm.users = []

//   vm.refresh = () => {
//     UsersService.fetchUsers()
//       .then((data) => {
//         vm.users = data
//       })
//   }
//   vm.refresh()

//   vm.select = (id) => {
//       if (vm.selected && vm.selected.id === id){
//         vm.selected = null
//       } else {
//          UsersService.fetchUserById(id).then((data) => {
//          vm.selected = data
//          vm.draft = {...vm.selected}
//         })
//       }
//       vm.cancelEditUser()
//     }

//   vm.save = (draft) => {
//     // vm.selected = UsersService.updateUser(draft)
//     UsersService.updateUser(draft)
//     vm.cancelEditUser()
//     vm.selected = null
//     vm.refresh()
//   }

//   vm.editUser = () => {
//     vm.isEditingUser = true
//   }

//   vm.cancelEditUser = () => {
//     vm.isEditingUser = false
//   }

//   vm.deleteUser = (id) => {
//     UsersService.deleteUser(id)
//     vm.refresh()
//     vm.selected = null
//     vm.cancelEditUser()
//   }
// })

angular.module("users.service", []).service("UsersService", function ($http) {
  this._users = [];

  this.fetchUsers = () => {
    return $http.get("http://localhost:3000/users").then((res) => {
      return (this._users = res.data);
    });
  };
  // this.fetchUsers();

  this.fetchUserById = (id) => {
    return $http.get(`http://localhost:3000/users/${id}`).then((res) => {
      return (this._users = res.data);
    });
  };

  this.updateUser = (draft) => {
    $http.put(`http://localhost:3000/users/${draft.id}`, JSON.stringify(draft));
  };

  this.deleteUser = (id) => {
    $http.delete(`http://localhost:3000/users/${id}`);
  };

  this.getUserById = (id) => {
    return this._users.find((u) => u.id == id);
  };
});
