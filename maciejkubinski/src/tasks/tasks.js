const tasks = angular.module("tasks", []);

angular
  .module("tasks")
  .component("tasksPage", {
    bindings: {},
    template: /* html */ `<div> TasksPage {{$ctrl.title}}
      <div class="row">
        <div class="col">
          <tasks-list tasks="$ctrl.tasks" 
                      on-select="$ctrl.select($event)"></tasks-list>
        </div>
        <div class="col">
          <task-details task="$ctrl.selected" on-cancel="$ctrl.cancel($event)" ng-if="$ctrl.selected">
          </task-details>

          <task-editor task="$ctrl.selected" ng-if="$ctrl.selected" on-save="$ctrl.save($event)"></task-editor>
        </div>
      </div>
    </div>`,
    controller($scope) {
      this.title = "Tasks";
      this.mode = "details";
      this.selected = null;
      this.tasks = [
        { id: 1, name: "task1" },
        { id: 2, name: "task2" },
        { id: 3, name: "task3" },
      ];
      this.select = (task) => {
        this.selected = task;
      };
      this.cancel = (event) => {
        this.selected = null;
        this.mode = "details";
      };
      this.edit = (event) => {
        this.mode = "edit";
      };
      this.save = (editedTask) => {
        this.tasks = this.tasks.map((task) => {
          if(task.id == editedTask.id){
            return task = editedTask
          } else return task
        })
      }
    },
  })
  .component('tasksList', {
      bindings: { tasks: '=', onSelect: "&" },
      template:/* html */`<div> List: <div class="list-group">
        <div class="list-group-item" ng-repeat="task in $ctrl.tasks"
          ng-class="{active: task.id ==  $ctrl.selected.id}"
          ng-click=" $ctrl.onSelect({$event: task}); $ctrl.selected = task"> 
        {{$index+1}}. {{task.name}}</div>
      </div></div>`,
      controller() {}, 
    },
  )
  .component("taskDetails", {
    bindings: { task: "=", onEdit: "&", onCancel: "&" },
    template: /* html */ `<div>tasksDetails <dl>
      <dt>Name</dt><dd>{{$ctrl.task.name}}</dd>
    </dl>
      <button ng-click="$ctrl.onEdit({$event: $ctrl.task})">Edit</button>
      <button ng-click="$ctrl.onCancel({$event:$ctrl.task})">Cancel</button>
    </div>`,
    controller() {},
  })
  .component("taskEditor", {
    bindings: { task: "<", onEdit: "&", onCancel: "&", onSave: "&" },
    template: /* html */ `<div> taskForm:
        <div class="form-group mb-3">
          <label for="task_name">Name</label>
          {{draft.name}}
          <input id="task_name" type="text" class="form-control" 
                ng-model="$ctrl.draft.name">
        </div>

        <div class="form-group mb-3">
          <label for="task_completed">
            <input id="task_completed" ng-model="$ctrl.draft.completed" type="checkbox"> Completed</label>
        </div>

        
        <button ng-click="$ctrl.onSave({$event: $ctrl.draft})">Save</button>
        <button ng-click="$ctrl.onCancel({$event:task, mode:'placki'})">Cancel</button>
      </div>`,
    controller($scope) {
      this.$onInit = () => {
        this.draft = { ...this.task };
      };
    },
  });