angular.module('config', [])

angular.module('myApp', ['config', "tasks", "users", "settings", 'ui.router'])

const app = angular.module('myApp')

app.config(function($stateProvider){

  $stateProvider
  .state({name:"users", url:"/users", template: "<h1>Users</h1>"})
  .state({name:"tasks", url:"/tasks", component:"tasksPage"})
  .state({name:"settings", url:"/settings", template: "<h1>settings</h1>"})
})

app.run(function ($rootScope) {

})


app.controller('AppCtrl', ($scope) => {
  $scope.user = {name:'Guest'}
  $scope.title = 'MyApp'
  $scope.isLoggedIn = false
  $scope.mode = 'users'
  $scope.visibleSettings = false

  $scope.toggleSettings = () => {
    $scope.visibleSettings = !$scope.visibleSettings
  }

  $scope.login = () => {
      $scope.user.name = "Admin"
      $scope.isLoggedIn = true
  }

  $scope.logout = () => {
      $scope.user.name = "Guest"
      $scope.isLoggedIn = false
  }

  $scope.setMode = (param) => {
    $scope.mode = param
  }

})


angular.bootstrap(document, ['myApp'])