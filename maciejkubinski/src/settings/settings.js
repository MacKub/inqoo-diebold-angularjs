const settings = angular.module("settings", [])



settings.controller("SettingsCtrl", ($scope) => {
  const vm = ($scope.SettingsCtrl = {});
  vm.test = "working"
});


settings.directive('appHighlight', function() {
return {
  restrict: 'EACM',
  link(scope, $element, attr, ctrl){
    $element.css('color', 'red').html('<p>placki </p>').on('click', event => {
      angular.element(event.currentTarget).css('color', 'blue')
    })
  }
}
})

settings.directive('appAlert', function () {
  return {
    scope: {message: '@', type: "@", onDismiss: '&'},
    template: /* html */`<div class="alert alert-{{type}}" ng-if="open">
    <span>{{message}}<span>
    <span class="close float-end" ng-click="close()">&times;</span>
    </div>`,
    controller($scope){
      
      $scope.open = true

      $scope.close = () => {
        $scope.open = false
        $scope.onDismiss()
      }
    }
  }
})